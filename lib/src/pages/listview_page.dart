import 'package:flutter/material.dart';

class ListaPage extends StatefulWidget {
  @override
  _ListaPageState createState() => _ListaPageState();
}

class _ListaPageState extends State<ListaPage> {

  List<int> _listaNumeros = [10,25,33,4,500];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Listas'),
      ),
      body: _crearLista()
          );
        }
      Widget _crearLista() {

        return ListView.builder(
          itemCount: _listaNumeros.length,
          itemBuilder: (BuildContext context, int index){

            final imagen = _listaNumeros[index];

            return  FadeInImage(
              image: NetworkImage('https://picsum.photos/id/${imagen}/500/300'),
              placeholder: AssetImage('assets/jar-loading.gif'),
            );
          },

        );
      }
}
      