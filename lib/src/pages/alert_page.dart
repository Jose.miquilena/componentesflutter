import 'package:flutter/material.dart';



class AlertPage extends StatelessWidget{
   @override
  Widget build(BuildContext context) {
    
    return Scaffold(
      appBar: AppBar( 
        title: Text('Alert Page'),
      ),
      body: Center(
        child: RaisedButton(child: Text('Mostrar Alerta'),
        color: Colors.blue,
        shape: StadiumBorder(), //bordes redondeados
        textColor: Colors.white,
        onPressed: ()=>_mostrarAlert(context),
        ),
      ),
      floatingActionButton: FloatingActionButton(onPressed: (){
        Navigator.pop(context);
      }, child: Icon(Icons.arrow_back),),
    );
  


  }


   void _mostrarAlert( BuildContext ctx){

     showDialog(

       context: ctx,
        barrierDismissible: true,
        builder: (ctx){

          return AlertDialog(

            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
            contentPadding: EdgeInsets.all(5.0),
            title:  Center(
                  child: Text('Titulo Alert')
              ),
            backgroundColor: Colors.black,
            elevation: 10.0,
            titleTextStyle: TextStyle(color: Colors.white, fontSize: 20.0),
            contentTextStyle: TextStyle(color: Colors.white),
            content: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
              Text('Este es el contenido de la alerta'),
              FlutterLogo( size: 95.0,)
             ],
            ),
            actions: <Widget>[
              FlatButton(child: Text('Cancelar'), onPressed: ()=> Navigator.of(ctx).pop()),
              FlatButton(child: Text('Ok'), onPressed: ()=> Navigator.of(ctx).pop())
            ],
            );

        }


     );

  }


}

 