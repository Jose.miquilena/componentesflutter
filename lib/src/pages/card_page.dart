import 'package:flutter/material.dart';

class CardPage extends StatelessWidget {
  const CardPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Cards'),
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            Navigator.pop(context);
          },
          child: Icon(
            Icons.arrow_back,
          ),
        ),
        body: ListView(
          padding: EdgeInsets.all(10.0),
          children: <Widget>[
            _cardTipo1(),
            SizedBox(
              height: 30.0,
            ),
            _cardTipo2(),
            SizedBox(
              height: 30.0,
            ),
            _cardTipo3(),
            SizedBox(
              height: 30.0,
            ),
             _cardTipo2(),
            SizedBox(
              height: 30.0,
            ), _cardTipo2(),
            SizedBox(
              height: 30.0,
            ), _cardTipo2(),
            SizedBox(
              height: 30.0,
            ), _cardTipo2(),
            SizedBox(
              height: 30.0,
            ), _cardTipo2(),
            SizedBox(
              height: 30.0,
            ),
          ],
        ));
  }

  Widget _cardTipo1() {
    return Card(
      elevation: 10.0,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
      child: Column(
        children: <Widget>[
          ListTile(
            leading: Icon(
              Icons.photo_album,
              color: Colors.blue,
            ),
            title: Text('Soy el titulo de esta tarjeta'),
            subtitle: Text('soy el titulo pequeño'),
          ),
          Row(
            children: <Widget>[
              FlatButton(
                child: Text('Cancelar'),
                onPressed: () {},
              ),
              FlatButton(
                child: Text('Ok'),
                onPressed: () {},
              )
            ],
            mainAxisAlignment: MainAxisAlignment.end,
          )
        ],
      ),
    );
  }

  Widget _cardTipo2() {
    final card = Container(
    
      //clipBehavior: Clip.antiAlias,
      child: Column(
        children: <Widget>[
          FadeInImage(
            image: NetworkImage(
                'https://www.schultzphoto.com/wp-content/uploads/2016/01/alaska-landscape-photography-038-highway-fall-road.jpg'),
            placeholder: AssetImage('assets/jar-loading.gif'),
            //fadeOutDuration: Duration(milliseconds: 2000),
            height: 300.0,
            fit: BoxFit.cover,
          ),

          Container(
            padding: EdgeInsets.all(7.0),
            child: Text('El gran camino de la vida'),
          )
        ],
      ),
    );

    return Container(
      decoration: BoxDecoration(borderRadius: BorderRadius.circular(30.0),
      color: Colors.white,
       boxShadow: <BoxShadow>[
         BoxShadow(
           color: Colors.black26,
           blurRadius: 10.0,
           spreadRadius: 2.0,
           offset:  Offset(2.0, -10.0)
           
           )
       ] ),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(30.0),
        child: card,
      ),
    );
  }

  Widget _cardTipo3() {
    return Card(
      child: Column(
        children: <Widget>[
          FadeInImage.assetNetwork(
            placeholder: 'assets/jar-loading.gif',
            height: 300,
            image:
                'https://www.schultzphoto.com/wp-content/uploads/2016/01/alaska-landscape-photography-038-highway-fall-road.jpg',
            fit: BoxFit.cover,
          ),
        ],
      ),
    );
  }
}
