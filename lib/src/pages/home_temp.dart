import 'package:flutter/material.dart';

class HomePageTemp extends  StatelessWidget{
 
  final opciones = [ 'Uno', 'Dos', 'Tres', 'Cuatro', 'Cinco'];



  @override
  Widget build(BuildContext context) {
    
    return Center(
      child: Scaffold(
        appBar: AppBar(
          title: Text('Componenetes Temp'),
        ) ,
        body: ListView(
         // children: _crearItems()
          children: _crearItemsCorta()
          
        ),
      ),
    );
  }

  List<Widget> _crearItems(){

    List<Widget> lista = new List<Widget>();
  
    for (String opt in opciones) {
      final tempWidget = ListTile(
        title: Text(opt),
        
      ); 
      lista..add(tempWidget)
           ..add(Divider(color: Colors.blue, indent:10.0, endIndent: 10.0,));

      
      
    }


    return lista;
  }

  List<Widget> _crearItemsCorta(){
    return  opciones.map( (item){
         return  Column(
           children: <Widget>[
             ListTile(
               title: Text(item + '!'),
               subtitle: Text('subtitle'),
               leading: Icon(Icons.account_balance),
               trailing: Icon(Icons.keyboard_arrow_right),
               onTap: (){},

             ),
             Divider()
           ],
         );
      }).toList();

      
  } 


}