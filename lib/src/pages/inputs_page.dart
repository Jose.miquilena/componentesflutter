

import 'package:flutter/material.dart';

import 'package:intl/intl.dart';

class InputsPage extends StatefulWidget {

_InputPageState createState() => _InputPageState();
}

String _nombre = '';
String _email = '';
String _password = '';
String _fechaNacimiento = '';

String _opcionSeleccionada = 'Básica';
List<String> _nivelAcademico = ['Básica', 'Secundaria', 'Universitaria','Post-Grado'];



TextEditingController _inputFieldDateController = new TextEditingController();

class _InputPageState extends State<InputsPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Inputs'),
      ),
      body: ListView( 
         children: <Widget>[
           _crearInput(),
           Divider(),
           _crearEmail(),
           Divider(),
           _crearDate(context),
           Divider(),
          _crearPassword(),
           Divider(),
           _crearDropdown(),
           Divider(),
           _crearPersona(),
             
         ],
          padding:  EdgeInsets.symmetric(horizontal: 10.0, vertical: 20.0),
      ),
      floatingActionButton: FloatingActionButton(
              onPressed: (){ Navigator.pop(context);},
               child: Icon(Icons.arrow_back),
      )
    );
    }
      
  Widget _crearInput() {
       
    return TextField(
        textCapitalization: TextCapitalization.sentences,
        decoration: InputDecoration(
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(10.0)),
          counter: Text('Letras ${_nombre.length}'),
          hintText: 'Nombre de la Persona',
          labelText: 'Nombre',
          helperText: 'Solo es el nombre',
          suffixIcon: Icon(Icons.accessibility),
          icon: Icon(Icons.account_circle)
        ),
        onChanged: (valor){
           setState(() {
             _nombre = valor;
           });
        },

    );

  }

  Widget _crearEmail() {

   return TextField(
       keyboardType: TextInputType.emailAddress,
        decoration: InputDecoration(
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(10.0)),
          
          hintText: 'Email',
          labelText: 'Email',
          suffixIcon: Icon(Icons.alternate_email),
          icon: Icon(Icons.email)
        ),
        onChanged: (valor){
           setState(() {
             _email = valor;
           });
        },

    );



   }

  Widget _crearPassword() {

   return TextField(
       obscureText: true,
        decoration: InputDecoration(
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(10.0)),
          
          hintText: 'Password',
          labelText: 'Password',
          suffixIcon: Icon(Icons.vpn_key),
          icon: Icon(Icons.lock)
        ),
        onChanged: (valor){
           setState(() {
             _password = valor;
           });
        },

    );



   }


  Widget _crearDate(BuildContext ctx) {
     return TextField(
       controller: _inputFieldDateController,
       enableInteractiveSelection: false,
        decoration: InputDecoration(
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(10.0)),
          
          hintText: 'Fecha de nacimiento',
          labelText: 'Fecha de nacimiento',
          suffixIcon: Icon(Icons.perm_contact_calendar),
          icon: Icon(Icons.calendar_today)
        ),
        onTap: (){

          FocusScope.of(ctx).requestFocus(new FocusNode());
          _selectDate(ctx);
                    
        },
        
          
      );
    }
          
  _selectDate(BuildContext ctx) async {

       DateTime picked =  await showDatePicker(
         context: ctx,
         initialDate: new DateTime.now(),
         firstDate:  new DateTime(2018),
         lastDate: new DateTime(2025),
         locale: Locale('es','ES')
         
       );

       if (picked != null){

         setState((){
           _fechaNacimiento =  DateFormat('d MMMM yyyy','ES').format(picked);
           
           _inputFieldDateController.text= _fechaNacimiento;
         });

       }

    }

  List<DropdownMenuItem<String>> getOptionsDropdown(){
      List<DropdownMenuItem<String>> lista = new List();
      _nivelAcademico.forEach((item)=>{

        lista.add(
          DropdownMenuItem(child: Text(item),value: item,)
        )

      });

      return lista;
  } 

  _crearDropdown() {

    return  Row(
      children: <Widget>[
      Icon(Icons.select_all),
      SizedBox(width: 30.0),
      Expanded(
            child: DropdownButton(
            value: _opcionSeleccionada,
            items: getOptionsDropdown(), 
            onChanged: (opt){
              setState(() {
                _opcionSeleccionada = opt;
              });
          },),
      )

    ],);
    
    
    }
                
     Widget _crearPersona(){
               return ListTile(
                 title: Text('Nombre es: $_nombre'),
                 subtitle: Text('Email es: $_email'),
                 trailing: Text('Nivel Academico: $_opcionSeleccionada'),
                 
                  
               );
     }
          
           
  
}
