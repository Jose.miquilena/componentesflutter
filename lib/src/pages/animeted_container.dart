
import 'dart:math';
import 'package:flutter/material.dart';



  class AnimetedContainerPage extends StatefulWidget {
    @override
    _AnimetedContainerPage createState() => _AnimetedContainerPage();
  }
  
  class _AnimetedContainerPage extends State<AnimetedContainerPage> {

    double _width = 50.0;
    double _height = 50.0;
    Color _color = Colors.pink;

    BorderRadiusGeometry _borderRadiusGeometry = BorderRadius.circular(8.0);



    @override
    Widget build(BuildContext context) {
      return Scaffold(
        appBar: AppBar(
          title: Text('Animated Container'),
        ),
        body: Center(
          child: AnimatedContainer(
            duration:  Duration(seconds: 1),
            curve: Curves.decelerate,
            width: _width,
            height: _height,
            decoration: BoxDecoration(
              borderRadius: _borderRadiusGeometry,
              color: _color
            ),
          )
        ),
        floatingActionButton: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
          FloatingActionButton( 
            heroTag: 'btn1',
            child: Icon( Icons.play_circle_outline), 
          onPressed: changeWidthHight ),
          SizedBox(width: 50),
         FloatingActionButton( 
           heroTag: 'btn2',
           child: Icon( Icons.arrow_back ), onPressed: () {Navigator.pop(context);} 
          ),
        ],)
        
      );
    }






   

   void  changeWidthHight(){
     
    final random = Random();


     setState(() {
         _width = random.nextInt(300).toDouble();
        _height = random.nextInt(100).toDouble();
        _color = Color.fromRGBO(
          random.nextInt(255),
          random.nextInt(255),
          random.nextInt(255),
          1);

          _borderRadiusGeometry = BorderRadius.circular(random.nextInt(100).toDouble());
     });

  }

  }

  