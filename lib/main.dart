



import 'package:flutter_localizations/flutter_localizations.dart';


import 'package:componentes/src/routes/routes.dart';
import 'package:flutter/material.dart';
 
void main() => runApp(MyApp());
 
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Componenetes App',
      debugShowCheckedModeBanner: false,
      //home: HomePage()
      initialRoute: '/',
      routes: getAplicationRoutes(),
      // onGenerateRoute: ( RouteSettings settings  ){
      //       print("ruta llamada: ${ settings.name }");

      //       return MaterialPageRoute(
      //         builder:  ( BuildContext context ) => AlertPage()
      //       );
      // },


      localizationsDelegates: [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
       
      ],
      supportedLocales: [
        const Locale('en','US'), // English
        const Locale('es','ES'), 
      ],
    );
  }
}